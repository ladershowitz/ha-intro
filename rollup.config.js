/* eslint-disable no-process-env */
/* global process */
import json from "@rollup/plugin-json";
import resolve from "@rollup/plugin-node-resolve";
import autoprefixer from "autoprefixer";
import postcss from "postcss";
import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import sass from "rollup-plugin-sass";
import { terser } from "rollup-plugin-terser";

const isProduction = process.env.BUILD === "production";

export default {
  input: "./src/index.js",
  output: {
    file: isProduction ? "./build/todo.min.js" : "./tmp/todo.js",
    format: "umd",
    sourcemap: true,
    name: "todo"
  },
  plugins: [
    babel({
      exclude: "node_modules/**"
    }),
    resolve({
      preferBuiltins: true,
      browser: true
    }),
    json(),
    commonjs(),
    isProduction && terser(),
    sass({
      output: true,
      processor: css =>
        postcss([autoprefixer])
          .process(css, { from: null })
          .then(result => result.css)
    })
  ]
};
